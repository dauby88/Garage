//
//  WorkItemHistoryViewController.swift
//  Garage
//
//  Created by Stuart on 4/20/16.
//  Copyright © 2016 Dauby Cafe. All rights reserved.
//

import UIKit
import CoreData

class WorkItemHistoryViewController: UIViewController, NSFetchedResultsControllerDelegate, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {

    // MARK: - Properties & Outlets
    
    var managedObjectContext: NSManagedObjectContext!
    var workItem: WorkItem! {
        didSet {
            navigationItem.title = "\(workItem.name) History"
        }
    }
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var editView: UIView!
    @IBOutlet var editViewConstraint: NSLayoutConstraint!

    @IBOutlet var mileIntervalField: UITextField! {
        didSet {
            if workItem.mileInterval != nil {
                mileIntervalField.text = String(workItem.mileInterval!)
            } else {
                mileIntervalField.text = nil
                mileIntervalField.enabled = false
            }
        }
    }
    @IBOutlet var monthIntervalField: UITextField!{
        didSet {
            if workItem.monthInterval != nil {
                monthIntervalField.text = String(workItem.monthInterval!)
            } else {
                monthIntervalField.text = nil
                monthIntervalField.enabled = false
            }
        }
    }
    @IBOutlet var trackedByMilesSwitch: UISwitch! {
        didSet {
            trackedByMilesSwitch.on = workItem.trackedByMiles.boolValue
        }
    }
    @IBOutlet var trackedByMonthsSwitch: UISwitch!{
        didSet {
            trackedByMonthsSwitch.on = workItem.trackedByMonths.boolValue
        }
    }
    
    @IBOutlet var itemSummaryLabel: UILabel! {
        didSet {
            updateSummaryString()
        }
    }
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Row height adjusts based on text size
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 80
        
        // Add targets for switches
        trackedByMilesSwitch.addTarget(self, action: #selector(milesSwitchChanged), forControlEvents: .ValueChanged)
        trackedByMonthsSwitch.addTarget(self, action: #selector(monthsSwitchChanged), forControlEvents: .ValueChanged)
        
        // Add toolbars to number pads
        addMilesToolbars()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        tableView.reloadData()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Simulate edit button press which safetly prevents saving of bad data
        if workItemEditing {
            editButtonPress(nil)
        }        
    }

    // MARK: - Editing the Work Item Tracking Stats
    
    // MARK: Edit Button
    var workItemEditing: Bool = false
    @IBOutlet var editButton: UIButton!
    @IBAction func editButtonPress(sender: AnyObject?) {
        
        if workItemEditing {
            // End editing session
            
            // Check for values on enabled fields
            if trackedByMilesSwitch.on && mileIntervalField.text == "" {
                // Change switch status
                trackedByMilesSwitch.on = false
            }
            
            if trackedByMonthsSwitch.on && monthIntervalField.text == "" {
                // Change switch status
                trackedByMonthsSwitch.on = false
            }
            
            // Save any changes
            workItem.trackedByMiles = trackedByMilesSwitch.on
            workItem.mileInterval = Int(mileIntervalField.text!)
            workItem.trackedByMonths = trackedByMonthsSwitch.on
            workItem.monthInterval = Int(monthIntervalField.text!)
            
            // Animate off screen
            animateEditSectionOffScreen()
            
            // Save the MOC
            let context = self.managedObjectContext
            CoreDataStack.save(context)
            
            // Update the summary string
            updateSummaryString()
            
            mileIntervalField.resignFirstResponder()
            monthIntervalField.resignFirstResponder()
            workItemEditing = false
        }
        else {
            // Begin editing session
            
            // Animate view to show user
            animateEditSectionOnScreen()
            
            workItemEditing = true
        }
    }
    
    // MARK: Switch Logic
    
    // Switches react to changes and immediately update the work item
    
    func milesSwitchChanged(theSwitch: UISwitch) {
        let switchOn = theSwitch.on

        if switchOn {
            workItem.trackedByMiles = true
            mileIntervalField.enabled = true
            mileIntervalField.becomeFirstResponder()
        } else {
            workItem.trackedByMiles = false
            workItem.mileInterval = nil
            mileIntervalField.text = nil
            mileIntervalField.enabled = false
        }
    }
    
    func monthsSwitchChanged(theSwitch: UISwitch) {
        let switchOn = theSwitch.on
        
        if switchOn {
            workItem.trackedByMonths = true
            monthIntervalField.enabled = true
            monthIntervalField.becomeFirstResponder()
        } else {
            workItem.trackedByMonths = false
            workItem.monthInterval = nil
            monthIntervalField.text = nil
            monthIntervalField.enabled = false
        }
    }

    
    // MARK: Animations
    
    func animateEditSectionOnScreen() {
        self.view.layoutIfNeeded()
        
        let height = editView.frame.height
        editViewConstraint.constant = height
        
        // Change icon to check button
        let image = UIImage(named: "CheckedIcon")
        editButton.setImage(image, forState: .Normal)
        
        UIView.animateWithDuration(0.25, animations: {
            
            self.view.layoutIfNeeded()
        })
    }
    
    func animateEditSectionOffScreen() {
        self.view.layoutIfNeeded()
        
        editViewConstraint.constant = 0
        
        // Change icon
        let image = UIImage(named: "MoreIcon")
        editButton.setImage(image, forState: .Normal)
        
        UIView.animateWithDuration(0.25, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    
    // MARK: - Segues
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "addEntry" {
            let controller = segue.destinationViewController as! AddWorkItemEntryViewController
            controller.workItem = self.workItem
            controller.managedObjectContext = self.managedObjectContext
        }
        if segue.identifier == "editEntry" {
            // Use sender so peek/pop works same as tap
            let cell = sender as! UITableViewCell
            let indexPath = tableView.indexPathForRowAtPoint(cell.center)!
            let entry = fetchedResultsController.objectAtIndexPath(indexPath) as! WorkItemEntry
            let controller = segue.destinationViewController as! EditWorkItemEntryViewController
            controller.workItemEntry = entry
            controller.managedObjectContext = self.managedObjectContext
        }
    }
    
    // MARK: - TableView Methods
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Remove item from the MOC
            let context = self.managedObjectContext!
            context.deleteObject(self.fetchedResultsController.objectAtIndexPath(indexPath) as! WorkItemEntry)
            // Save the MOC
            do {
                try context.save()
            } catch {
                print("Error deleting object: \(error)")
            }
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.fetchedResultsController.sections?.count ?? 0
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionInfo = self.fetchedResultsController.sections![section]
        return sectionInfo.numberOfObjects
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("WorkItemEntryCell", forIndexPath: indexPath) as! WorkItemEntryCell
        let workItemEntry = self.fetchedResultsController.objectAtIndexPath(indexPath) as! WorkItemEntry
        cell.updateLabels(workItemEntry)
        return cell
    }

    // MARK: - Fetched results controller
    
    var fetchedResultsController: NSFetchedResultsController {
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let fetchRequest = NSFetchRequest()
        // Entity is WorkItem
        let entity = NSEntityDescription.entityForName("WorkItemEntry", inManagedObjectContext: self.managedObjectContext!)
        fetchRequest.entity = entity
        
        // Set the batch size to a suitable number.
        fetchRequest.fetchBatchSize = 20
        
        // Sort by date added, predicate is tied to the workItem
        let sortDescriptor = NSSortDescriptor(key: "dateCompleted", ascending: false)
        let fetchPredicate = NSPredicate(format: "workItem == %@", self.workItem)
        
        fetchRequest.sortDescriptors = [sortDescriptor]
        fetchRequest.predicate = fetchPredicate
        
        // No sections and no cache to set
        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: self.managedObjectContext!, sectionNameKeyPath: nil, cacheName: nil)
        aFetchedResultsController.delegate = self
        _fetchedResultsController = aFetchedResultsController
        
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            print("Unresolved error \(error)")
        }
        
        return _fetchedResultsController!
    }
    var _fetchedResultsController: NSFetchedResultsController? = nil
    
    func controllerWillChangeContent(controller: NSFetchedResultsController) {
        self.tableView.beginUpdates()
    }
    
    func controller(controller: NSFetchedResultsController, didChangeSection sectionInfo: NSFetchedResultsSectionInfo, atIndex sectionIndex: Int, forChangeType type: NSFetchedResultsChangeType) {
        switch type {
        case .Insert:
            self.tableView.insertSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Fade)
        case .Delete:
            self.tableView.deleteSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Fade)
        default:
            return
        }
    }
    
    func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?) {
        switch type {
        case .Insert:
            tableView.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: .Fade)
            tableView.reloadData()
        case .Delete:
            tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: .Fade)
        case .Update:
            tableView.reloadRowsAtIndexPaths([indexPath!], withRowAnimation: .Fade)
        case .Move:
            tableView.moveRowAtIndexPath(indexPath!, toIndexPath: newIndexPath!)
            tableView.reloadData()
        }
    }
    
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        self.tableView.endUpdates()
    }
    
    /*
     // Implementing the above methods to update the table view in response to individual changes may have performance implications if a large number of changes are made simultaneously. If this proves to be an issue, you can instead just implement controllerDidChangeContent: which notifies the delegate that all section and object changes have been processed.
     
     func controllerDidChangeContent(controller: NSFetchedResultsController) {
     // In the simplest, most efficient, case, reload the table view.
     self.tableView.reloadData()
     }
     */
    
    // MARK: - Helper Methods
    
    func updateSummaryString() {
        let trackedByMiles = workItem.trackedByMiles.boolValue
        let trackedByMonths = workItem.trackedByMonths.boolValue
        var summary: String
        
        if trackedByMiles && trackedByMonths {
            let mileInterval = milesFormatter.stringFromNumber(workItem.mileInterval!)
            summary = "Due every \(mileInterval!) miles or \(workItem.monthInterval!) months"
        }
        else if trackedByMiles {
            let mileIntervalString = workItem.mileInterval!.milesString
            summary = "Due every \(mileIntervalString)"
        }
        else if trackedByMonths {
            summary = "Due every \(workItem.monthInterval!) months"
        }
        else {
            summary = "This item is not being tracked"
        }
        
        itemSummaryLabel.text = summary
    }
    
    // Create toolbar for number text fields
    func addMilesToolbars() {
        let width = view.frame.size.width
        let toolbarRect = CGRectMake(0, 0, width, 50)
        let numberToolbar = UIToolbar(frame: toolbarRect)
        numberToolbar.barStyle = .Default
        let nextButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(resignAllResponders))
        nextButton.tintColor = UIColor.appNavyBlueColor()
        numberToolbar.items = [
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil),
            nextButton]
        numberToolbar.sizeToFit()
        mileIntervalField.inputAccessoryView = numberToolbar
        monthIntervalField.inputAccessoryView = numberToolbar
    }
    
    func resignAllResponders() {
        mileIntervalField.resignFirstResponder()
        monthIntervalField.resignFirstResponder()
    }
}
