//
//  CarCell.swift
//  Garage
//
//  Created by Stuart on 4/22/16.
//  Copyright © 2016 Dauby Cafe. All rights reserved.
//

import UIKit

class CarCell: UITableViewCell {
    
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var rawMilesLabel: UILabel!
    @IBOutlet var estMilesLabel: UILabel!
    @IBOutlet var nextWorkItemLabel: UILabel!
    @IBOutlet var statusView: UIView!
    
    @IBOutlet var estMileLabelHeightConstraint: NSLayoutConstraint!
    
    // Custom highlighting so the status dot isn't covered on selection
    override func setHighlighted(highlighted: Bool, animated: Bool) {
        //super.setHighlighted(highlighted, animated: animated)
        if (highlighted) {
            self.contentView.superview?.backgroundColor = UIColor.reallyLightGrayColor()
        }
        else {
            self.contentView.superview?.backgroundColor = UIColor.clearColor()
        }
    }
    
    func updateLabels(car: Car) {
        // Configure cell with the car
        
        let title1Font = UIFont.preferredFontForTextStyle(UIFontTextStyleTitle1)
        let title3Font = UIFont.preferredFontForTextStyle(UIFontTextStyleTitle3)
        let bodyFont = UIFont.preferredFontForTextStyle(UIFontTextStyleBody)
//        let calloutFont = UIFont.preferredFontForTextStyle(UIFontTextStyleCallout)

        // Car name
        nameLabel.text = car.name
        nameLabel.font = title1Font
        
        // Miles on the car
        rawMilesLabel.text = car.rawMiles.milesString
        rawMilesLabel.textColor = UIColor.darkGrayColor()
        rawMilesLabel.font = title3Font
        
        estMilesLabel.text = car.estimatedMiles.estMilesString
        estMilesLabel.font = title3Font
        
        if ((car.rawMiles.integerValue + 1000) > car.estimatedMiles.integerValue) {
            estMilesLabel.hidden = true
            estMileLabelHeightConstraint.constant = 8
        }
        else {
            estMilesLabel.textColor = UIColor.lightGrayColor()
            estMileLabelHeightConstraint.constant = 8 + 8 + estMilesLabel.frame.height
        }
        
        
        // Next work due on the car
        nextWorkItemLabel.font = bodyFont
        if car.nextWorkDue != nil {
            let nextDue = car.nextWorkDue!
            let nextDueName = nextDue.name
            let daysDue = nextDue.approxDaysUntilDue
            
            // Set text for the label
            // Item not yet due
            if daysDue! < 1 {
                // Item overdue
                nextWorkItemLabel.text = "\(nextDueName) is due!!!"
                nextWorkItemLabel.textColor = UIColor.statusRedColor()
                statusView.backgroundColor = UIColor.statusRedColor()
            }
            else if daysDue! < 60 {
                let dueString = daysDue!.dayWkMonString // Includes the unit word
                nextWorkItemLabel.text = "\(nextDueName) due in \(dueString)"
                nextWorkItemLabel.textColor = UIColor.darkGrayColor()
                statusView.backgroundColor = UIColor.statusYellowColor()
            }
            else {
                let dueString = daysDue!.dayWkMonString // Includes the unit word
                nextWorkItemLabel.text = "\(nextDueName) due in \(dueString)"
                nextWorkItemLabel.textColor = UIColor.darkGrayColor()
                statusView.backgroundColor = UIColor.statusGreenColor()
            }
        }
        else {
            // No history, default text
            nextWorkItemLabel.text = "No Items Due"
            nextWorkItemLabel.textColor = UIColor.darkGrayColor()
            statusView.backgroundColor = UIColor.lightGrayColor()
        }
    }
}