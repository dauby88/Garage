//
//  NotificationStack.swift
//  Garage
//
//  Created by Stuart on 6/3/16.
//  Copyright © 2016 Dauby Cafe. All rights reserved.
//

import UIKit
import CoreData

class NotificationStack {
    
    class func scheduleLocalNotifications(ForCar car: Car) {
        // Clear all current notifications
        UIApplication.sharedApplication().cancelAllLocalNotifications()
        
        let settings = UIApplication.sharedApplication().currentUserNotificationSettings()
        
        if settings?.types == .None {
            // Can't schedule 
            // Either we don't have permission to schedule notifications, or we haven't asked yet.
            
            return
        }
        
        // Create array of all work items sorted by due date
        var allWorkItems = [WorkItem]()
        for item in car.workItems {
            allWorkItems.append(item as! WorkItem)
        }
        
        // Schedule notification for all work items that have a due date
        if !allWorkItems.isEmpty {
            allWorkItems.sortInPlace( { $0.approxDaysUntilDue < $1.approxDaysUntilDue } )
            for workItem in allWorkItems {
                if workItem.approxDaysUntilDue != nil {
                    if workItem.approxDaysUntilDue < 1 {
                        // Due now, don't schedule anything
                    }
                    else if workItem.approxDaysUntilDue < 7 {
                        // Due in less than a week
                        // Schedule for 1 day before
                        scheduleNotificationFor(WorkItem: workItem, daysBefore: 1)
                    }
                    else {
                        // Due in more than a week
                        // Schedule for 1 day before and 1 week before
                        scheduleNotificationFor(WorkItem: workItem, daysBefore: 1)
                        scheduleNotificationFor(WorkItem: workItem, daysBefore: 7)
                    }
                }
            }
        }
    }
    
    class private func scheduleNotificationFor(WorkItem item: WorkItem, daysBefore: Int) {
        let notification = UILocalNotification()
        
        // Calculate fire date
        let daysUntilDue = item.approxDaysUntilDue!
        // Start with today's date
        let fireDateComponents = NSCalendar.currentCalendar().components([.Year, .Month, .Day], fromDate: NSDate())
        // Add the days until due
        fireDateComponents.day += daysUntilDue
        // Subtract days before to get the notification day
        fireDateComponents.day -= daysBefore
        
        // Set notification time
        // 10:00 AM
        fireDateComponents.hour = 10
        fireDateComponents.minute = 0
        
        let fireDate = NSCalendar.currentCalendar().dateFromComponents(fireDateComponents)
        notification.fireDate = fireDate

        let itemName = item.name
        let carName = item.car!.name
        
        let timeString: String
        switch daysBefore {
        case 1:
            timeString = "tomorrow"
        case 7:
            timeString = "in 1 week"
        default:
            timeString = "\(daysBefore) days"
        }
        
        
        notification.alertBody = "\(itemName) due for \(carName) \(timeString)"
        notification.alertAction = "open Garage Keeper"
        notification.soundName = UILocalNotificationDefaultSoundName
        notification.userInfo = ["WorkItemName ": itemName]
        
        UIApplication.sharedApplication().scheduleLocalNotification(notification)
    }
}