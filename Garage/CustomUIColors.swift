//
//  CustomUIColors.swift
//  Garage
//
//  Created by Stuart on 5/4/16.
//  Copyright © 2016 Dauby Cafe. All rights reserved.
//

import UIKit

extension UIColor {
    class func appNavyBlueColor() -> UIColor {
        // Navy blue color used throughout the app
        // RGB: 73, 110, 156
        let red: Float = 73.0/255.0
        let green: Float = 110.0/255.0
        let blue: Float = 156.0/255.0
        
        return UIColor.init(colorLiteralRed: red, green: green, blue: blue, alpha: 1.0)
    }
    
    class func statusRedColor() -> UIColor {
        // http://www.flatuicolorpicker.com/
        // Tall Poppy
        // RGB: 192, 57, 43
        let red: Float = 192.0/255.0
        let green: Float = 57.0/255.0
        let blue: Float = 43.0/255.0
        
        return UIColor.init(colorLiteralRed: red, green: green, blue: blue, alpha: 1.0)
    }
    
    class func statusGreenColor() -> UIColor {
        // http://www.flatuicolorpicker.com/
        // EUCALYPTUS
        // RGB: 38, 166, 91
        let red: Float = 38.0/255.0
        let green: Float = 166.0/255.0
        let blue: Float = 91.0/255.0
        
        return UIColor.init(colorLiteralRed: red, green: green, blue: blue, alpha: 1.0)
    }
    
    class func statusYellowColor() -> UIColor {
        // http://www.flatuicolorpicker.com/
        // Ripe Lemon
        // RGB: 247, 202, 24
        let red: Float = 247.0/255.0
        let green: Float = 202.0/255.0
        let blue: Float = 24.0/255.0
        
        return UIColor.init(colorLiteralRed: red, green: green, blue: blue, alpha: 1.0)
    }
    
    class func reallyLightGrayColor() -> UIColor {
        let normalGrayColor = UIColor.grayColor()
        return normalGrayColor.colorWithAlphaComponent(0.15)
    }
    
    class func borderColor() -> UIColor {
        // Same color as default UITextField
        let red: Float = 204.0/255.0
        let green: Float = 204.0/255.0
        let blue: Float = 204.0/255.0
        
        return UIColor.init(colorLiteralRed: red, green: green, blue: blue, alpha: 1.0)
    }
}