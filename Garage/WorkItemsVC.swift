//
//  WorkItemsViewController.swift
//  Garage
//
//  Created by Stuart on 4/17/16.
//  Copyright © 2016 Dauby Cafe. All rights reserved.
//

import UIKit
import CoreData

class WorkItemsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate, AddWorkItemsViewControllerDelegate {
    
    @IBOutlet var tableView: UITableView!
    
    @IBOutlet var carMilesButton: UIButton!

    var managedObjectContext: NSManagedObjectContext!
    
    var car: Car! {
        didSet {
            let carName = car.name
            navigationItem.title = carName
        }
    }
    
    // MARK: - Quick Miles Update
    
    @IBAction func quickUpdateMiles(sender: AnyObject) {
        let title = "Update \(car.name) Miles"
        let ac = UIAlertController(title: title, message: "", preferredStyle: .Alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
        let updateAction = UIAlertAction(title: "Update", style: .Default, handler: { (action) -> Void in
            let mileageField = ac.textFields![0] as UITextField
            if let newMileage = Int(mileageField.text!) {
                self.car.miles = newMileage
            }
            self.carMilesButton.setTitle(self.car.miles.milesString, forState: .Normal)
            
            // Recalculate estimates and reload table data
            self.car.updateNextWorkDue()
            self.tableView.reloadData()
        })
        
        ac.addTextFieldWithConfigurationHandler({(textField) -> Void in
            textField.placeholder = "Current Mileage"
            textField.keyboardType = .DecimalPad
        })
        ac.addAction(cancelAction)
        ac.addAction(updateAction)
        
        presentViewController(ac, animated: true, completion: nil)
    }
    
    // MARK: - View Lifecycle
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        // Update stats for next time the work item is due
        // Need to calculate here because the values are not persisted in core data
        updateWorkItemDueStats()
        
        // Update car miles label
        carMilesButton.setTitle(car.miles.milesString, forState: .Normal)
        // Reload tableView
        tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Update car miles label
        carMilesButton.setTitle(car.miles.milesString, forState: .Normal)
        
        // Row height adjusts based on text size
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 85
        
        tableView.tableFooterView = UIView(frame: CGRectZero)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // If the triggered segue is "AddWorkItem"
        if segue.identifier == "addWorkItems" {
            // Pass the car to the add work item controller
            let car = self.car
            let navController = segue.destinationViewController as! UINavigationController
            let controller = navController.topViewController as! AddWorkItemsViewController
            controller.delegate = self
            controller.car = car
            controller.managedObjectContext = self.managedObjectContext
        }
        
        // If triggered segue is "showHistory"
        else if segue.identifier == "showHistory" {
            // Use sender so peek/pop works same as tap
            let cell = sender as! UITableViewCell
            let indexPath = tableView.indexPathForRowAtPoint(cell.center)!
            let workItem = fetchedResultsController.objectAtIndexPath(indexPath)

            let controller = segue.destinationViewController as! WorkItemHistoryViewController
            controller.workItem = workItem as! WorkItem
            controller.managedObjectContext = self.managedObjectContext
        }
        
        else if segue.identifier == "addEntryShortcut" {
            // Sender is index path of work item
            let indexPath = sender as! NSIndexPath
            let workItem = fetchedResultsController.objectAtIndexPath(indexPath) as! WorkItem
            let controller = segue.destinationViewController as! AddWorkItemEntryViewController
            controller.workItem = workItem
            controller.managedObjectContext = self.managedObjectContext
        }
    }
    
    // MARK: - Helper Methods
    
    func updateWorkItemDueStats() {
        for object in car.workItems {
            if let item = object as? WorkItem {
                item.updateNextDue()
            }
        }
    }
    
    // MARK: - TableView Methods
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.fetchedResultsController.sections?.count ?? 0
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionInfo = self.fetchedResultsController.sections![section]
        return sectionInfo.numberOfObjects
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("WorkItemCell", forIndexPath: indexPath) as! WorkItemCell
        let workItem = self.fetchedResultsController.objectAtIndexPath(indexPath) as! WorkItem

        cell.updateLabels(workItem)
        return cell
    }
    
    func configureCell(cell: UITableViewCell, withWorkItem workItem: WorkItem) {
        cell.textLabel!.text = workItem.name
    }
    
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        // Delete Action
        let deleteAction = UITableViewRowAction(style: .Destructive, title: "Delete" , handler: { (action:UITableViewRowAction!, indexPath:NSIndexPath!) -> Void in
            // Delete the work item
            self.deleteItem(forRowAtIndexPath: indexPath)
        })
        
        // Edit Action
        let addEntryAction = UITableViewRowAction(style: .Normal, title: "Add Entry" , handler: { (action:UITableViewRowAction!, indexPath:NSIndexPath!) -> Void in
            // Open the new entry view controller
            self.performSegueWithIdentifier("addEntryShortcut", sender: indexPath)
        })
        addEntryAction.backgroundColor = UIColor.blueColor()

        
        return [deleteAction, addEntryAction]
    }
    
    func deleteItem(forRowAtIndexPath indexPath: NSIndexPath) {
        let item = fetchedResultsController.objectAtIndexPath(indexPath) as! WorkItem
        let title = "Delete \(item.name)"
        let message = "Are you sure you want to stop tracking this item? All history will be deleted. You cannot undo this action."
        let ac = UIAlertController(title: title, message: message, preferredStyle: .ActionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
        ac.addAction(cancelAction)
        
        let deleteAction = UIAlertAction(title: "Delete", style: .Destructive, handler: { (action) -> Void in
            // Remove item from the MOC
            let context = self.managedObjectContext!
            context.deleteObject(self.fetchedResultsController.objectAtIndexPath(indexPath) as! WorkItem)
            // Save the MOC
            CoreDataStack.save(context)
            
        })
        ac.addAction(deleteAction)
        
        // Present the alert controller
        presentViewController(ac, animated: true, completion: nil)
    }

    
    // MARK: - AddWorkItemViewController Delegate Methods
    
    func addWorkItemsViewControllerDidSave() {
        // Save the context
        CoreDataStack.save(managedObjectContext)
        
        // Dismiss View Controller
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: - Fetched results controller
    
    var fetchedResultsController: NSFetchedResultsController {
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let fetchRequest = NSFetchRequest()
        // Entity is WorkItem
        let entity = NSEntityDescription.entityForName("WorkItem", inManagedObjectContext: self.managedObjectContext!)
        fetchRequest.entity = entity
        
        // Set the batch size to a suitable number.
        fetchRequest.fetchBatchSize = 20
        
        // Sort by name, predicate is tied to the car
        let sortDescriptor = NSSortDescriptor(key: "name", ascending: true)
        let fetchPredicate = NSPredicate(format: "car == %@", self.car)
        
        fetchRequest.sortDescriptors = [sortDescriptor]
        fetchRequest.predicate = fetchPredicate
        
        // No sections and no cache to set
        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: self.managedObjectContext!, sectionNameKeyPath: nil, cacheName: nil)
        aFetchedResultsController.delegate = self
        _fetchedResultsController = aFetchedResultsController
        
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            print("Unresolved error \(error)")
        }
        
        return _fetchedResultsController!
    }
    var _fetchedResultsController: NSFetchedResultsController? = nil
    
    func controllerWillChangeContent(controller: NSFetchedResultsController) {
        self.tableView.beginUpdates()
    }
    
    func controller(controller: NSFetchedResultsController, didChangeSection sectionInfo: NSFetchedResultsSectionInfo, atIndex sectionIndex: Int, forChangeType type: NSFetchedResultsChangeType) {
        switch type {
        case .Insert:
            self.tableView.insertSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Fade)
        case .Delete:
            self.tableView.deleteSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Fade)
        default:
            return
        }
    }
    
    func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?) {
        switch type {
        case .Insert:
            tableView.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: .Fade)
            tableView.reloadData()
        case .Delete:
            tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: .Fade)
        case .Update:
            tableView.reloadRowsAtIndexPaths([indexPath!], withRowAnimation: .Fade)
        case .Move:
            tableView.moveRowAtIndexPath(indexPath!, toIndexPath: newIndexPath!)
            tableView.reloadData()
        }
    }
    
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        self.tableView.endUpdates()
    }
    
    /*
     // Implementing the above methods to update the table view in response to individual changes may have performance implications if a large number of changes are made simultaneously. If this proves to be an issue, you can instead just implement controllerDidChangeContent: which notifies the delegate that all section and object changes have been processed.
     
     func controllerDidChangeContent(controller: NSFetchedResultsController) {
     // In the simplest, most efficient, case, reload the table view.
     self.tableView.reloadData()
     }
     */
}


