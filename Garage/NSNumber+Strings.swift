//
//  NSNumber+MilesString.swift
//  Garage
//
//  Created by Stuart on 5/4/16.
//  Copyright © 2016 Dauby Cafe. All rights reserved.
//

import Foundation

extension NSNumber {
    var milesString: String {
        if let formattedString = milesFormatter.stringFromNumber(self) {
            return "\(formattedString) miles"
        } else {
            return "xxx,xxx miles"
        }
    }
    var estMilesString: String {
        if let formattedString = milesFormatter.stringFromNumber(self) {
            return "\(formattedString) est. miles"
        } else {
            return "xxx,xxx miles"
        }
    }
}