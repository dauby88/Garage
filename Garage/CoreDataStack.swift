//
//  CoreDataStack.swift
//  Garage
//
//  Created by Stuart on 5/14/16.
//  Copyright © 2016 Dauby Cafe. All rights reserved.
//

import Foundation
import CoreData

class CoreDataStack {
    
    class func save(context: NSManagedObjectContext) {
        // Save the context
        do {
            try context.save()
        } catch {
            print("Failure to save context: \(error)")
        }
    }
}
