//
//  AppInfoVC.swift
//  Garage
//
//  Created by Stuart on 5/7/16.
//  Copyright © 2016 Dauby Cafe. All rights reserved.
//

import UIKit
import MessageUI
import StoreKit

class AppInfoViewController: UITableViewController, MFMailComposeViewControllerDelegate, SKStoreProductViewControllerDelegate {
    
    // MARK: - Properties & Outlets
    
    @IBOutlet var contactDevCell: UITableViewCell!
    @IBOutlet var iconCreditCell: UITableViewCell!
    @IBOutlet var rateAppCell: UITableViewCell!
    
    @IBAction func doneButtonPressed(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Allow iconCreditCell text to wrap to 2 lines
        iconCreditCell.textLabel?.numberOfLines = 0
        iconCreditCell.textLabel?.lineBreakMode = .ByWordWrapping
    }
    
    // MARK: - TableView Methods
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let selectedCell = tableView.cellForRowAtIndexPath(indexPath)
        if selectedCell == contactDevCell {
            // Open email compose view to email developer
            sendEmailButtonTapped()
        }
        else if selectedCell == rateAppCell {
            // Open App Store link
            let spinner = UIActivityIndicatorView()
            spinner.frame = CGRectMake(0, 0, 24, 24)
            selectedCell?.accessoryView = spinner
            spinner.startAnimating()
            
            rateThisAppButtonTapped()
        }
    }
    
    // MARK: - Email Developer View
    
    func sendEmailButtonTapped() {
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.presentViewController(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
    }
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setToRecipients(["dauby88@gmail.com"])
        mailComposerVC.setSubject("Support Request for Garage App")
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        let sendMailErrorAlert = UIAlertController(title: "Could Not Send Email", message: "Your device could not send e-mail", preferredStyle: .Alert)
        
        presentViewController(sendMailErrorAlert, animated: true, completion: nil)
    }
    
    // MARK: MFMailComposeViewControllerDelegate Method
    
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: - App Store View
    
    func rateThisAppButtonTapped() {
        let appStoreID = "1114336777"
        openStoreProductWithiTunesItemIdentifier(appStoreID)
    }
    
    func openStoreProductWithiTunesItemIdentifier(identifier: String) {
        let storeViewController = SKStoreProductViewController()
        storeViewController.delegate = self
        
        let parameters = [SKStoreProductParameterITunesItemIdentifier : identifier]
        storeViewController.loadProductWithParameters(parameters) { [weak self] (loaded, error) -> Void in
            if loaded {
                // Parent class of self is UIViewContorller
                self?.presentViewController(storeViewController, animated: true, completion: nil)
            }
        }
    }
    
    // MARK: SKStoreProductViewControllerDelegate Method
    
    func productViewControllerDidFinish(viewController: SKStoreProductViewController) {
        // Deselect cells and remove accessory view (spinner)
        if let indexPaths = tableView.indexPathsForSelectedRows {
            for path in indexPaths {
                tableView.deselectRowAtIndexPath(path, animated: false)
                let cell = tableView.cellForRowAtIndexPath(path)
                cell?.accessoryView = nil
            }
        }
        
        viewController.dismissViewControllerAnimated(true, completion: nil)
    }
}
