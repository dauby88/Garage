//
//  DefaultWorkItems.swift
//  Garage
//
//  Created by Stuart on 5/10/16.
//  Copyright © 2016 Dauby Cafe. All rights reserved.
//

import UIKit

class DefaultWorkItem: NSObject {
    
    var name: String
    var trackedByMiles: Bool
    var mileInterval: Int?
    var trackedByMonths: Bool
    var monthInterval: Int?
    
    init(withName name: String, trackingByMiles trackedByMiles: Bool, miles mileInterval: Int?,trackingByMonths trackedByMonths: Bool,months monthInterval: Int?) {
        self.name = name
        self.trackedByMiles = trackedByMiles
        self.mileInterval = mileInterval
        self.trackedByMonths = trackedByMonths
        self.monthInterval = monthInterval
    }
    
    required init?(coder aDecoder: NSCoder) {
        name = aDecoder.decodeObjectForKey("name") as! String
        trackedByMiles = aDecoder.decodeObjectForKey("trackedByMiles") as! Bool
        mileInterval = aDecoder.decodeObjectForKey("mileInterval") as! Int?
        trackedByMonths = aDecoder.decodeObjectForKey("trackedByMonths") as! Bool
        monthInterval = aDecoder.decodeObjectForKey("monthInterval") as! Int?   
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(name, forKey: "name")
        aCoder.encodeObject(trackedByMiles, forKey: "trackedByMiles")
        aCoder.encodeObject(mileInterval, forKey: "mileInterval")
        aCoder.encodeObject(trackedByMonths, forKey: "trackedByMonths")
        aCoder.encodeObject(monthInterval, forKey: "monthInterval")
    }
    
    class func createStandardSetOfDefaults() {
        // Creates standard set of defaults
        // Called in app delegate if no defaults exist 
        
        var defaultItems: [DefaultWorkItem] = Array()
        var defaultObjects: [AnyObject] = Array()
        
        let name: [String]        = ["Oil Change", "Air Filter", "Tire Rotation", "Timing Belt", "Belts", "Windshield Wipers", "Coolant Flush", "Spark Plugs"]
        let trackByMiles: [Bool]  = [true        , true        , true           , true         , true   , false              , true           , true         ]
        let mileInterval: [Int?]  = [6000        , 15000       , 5000           , 100000       , 30000  , nil                , 12000          , 100000       ]
        let trackByMonths: [Bool] = [true        , true        , true           , false        , true   , true               , true           , false        ]
        let monthInterval: [Int?] = [6           , 12          , 8              , nil          , 24     , 12                 , 12             , nil          ]
        
        let numberOfDefaults = name.count
    
        for index in 0...(numberOfDefaults - 1) {
            let defaultItem = DefaultWorkItem(withName: name[index], trackingByMiles: trackByMiles[index], miles: mileInterval[index], trackingByMonths: trackByMonths[index], months: monthInterval[index])
            defaultItems.append(defaultItem)
        }
        
        // Sort array alphabetically
        defaultItems.sortInPlace( { $0.name < $1.name } )

        for item in defaultItems {
            let encodedItem = NSKeyedArchiver.archivedDataWithRootObject(item)
            defaultObjects.append(encodedItem)
        }
 
        NSUserDefaults.standardUserDefaults().setObject(defaultObjects, forKey: "defaultItems")
    }
    
    class func sortDefaultWorkItems() {
        // Sorts defaultWorkItems array for NSUserDefaults
        let defaultObjects = NSUserDefaults.standardUserDefaults().arrayForKey("defaultItems")
        var defaultItems = [DefaultWorkItem]()
        var sortedDefaultObjects = [AnyObject]()
        
        // Safetly unwrap array
        if let defaultObjects = defaultObjects {
            // Iterate through the objects
            for object in defaultObjects {
                // Unwrap the objects
                if let object = object as? NSData {
                    // Unarchive, convert them to items, and add to array
                    let item = NSKeyedUnarchiver.unarchiveObjectWithData(object) as! DefaultWorkItem
                    defaultItems.append(item)
                }
            }
            // Sort array in place
            defaultItems.sortInPlace( { $0.name < $1.name } )
            
            // Encode items in order
            for item in defaultItems {
                let encodedItem = NSKeyedArchiver.archivedDataWithRootObject(item)
                sortedDefaultObjects.append(encodedItem)
            }
            // Save to NSUserDefaults
            NSUserDefaults.standardUserDefaults().setObject(sortedDefaultObjects, forKey: "defaultItems")
        }
    }
}
