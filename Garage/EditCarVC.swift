//
//  EditCarVC.swift
//  Garage
//
//  Created by Stuart on 5/10/16.
//  Copyright © 2016 Dauby Cafe. All rights reserved.
//

import UIKit
import CoreData

protocol EditCarViewControllerDelegate {
    func editCarViewControllerDidSave()
    func editCarViewControllerDidCancel()
}


class EditCarViewController: UIViewController, UITextFieldDelegate {
    
    // MARK: - Properties & Outlets
    
    var car: Car!
    var managedObjectContext: NSManagedObjectContext!
    var delegate: EditCarViewControllerDelegate!
    
    @IBOutlet var scrollView: UIScrollView!
    
    @IBOutlet var nameField: UITextField! {
        didSet {
            nameField.text = car.name
        }
    }
    @IBOutlet var milesField: UITextField! {
        didSet {
            milesField.text = String(car.miles)
        }
    }
    @IBOutlet var milesPerDayField: UITextField! {
        didSet {
            milesPerDayField.text = String(car.milesPerDay)
        }
    }
    @IBOutlet var milesTimeIntervalControl: UISegmentedControl!
    
    @IBAction func saveButtonTapped() {
        // Save and dismiss view
        save()
    }
    
    func save() {
        // Check that all entries are valid
        if textFieldsAreValid() {
            // Update the car
            car.name = nameField!.text!
            let miles = milesFormatter.numberFromString(milesField!.text!)!
            // Miles setter also updates date of last miles update
            car.miles = miles.integerValue
            let milesPerDay = convertMilesToDailyValue()
            if milesPerDay < 1 {
                car.milesPerDay = 1
            } else {
                car.milesPerDay = milesPerDay
            }
            
            resignAllFirstResponders()
            delegate.editCarViewControllerDidSave()
        }
        else {
            return
        }
    }
    
    
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set nav bar title
        let navBar = self.navigationItem
        navBar.title = "\(car.name) Details"
        
        // Coordinate Text Fields
        addMilesToolbar()
        addMilesPerDayToolbar()
    }
    
    override func viewWillDisappear(animated: Bool) {
        save()
        
        super.viewWillDisappear(animated)
    }
    
    // MARK: - TextField Delegate Methods
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        if textField == nameField {
            milesField.becomeFirstResponder()
        }
        
        return false // We do not want UITextField to insert line-breaks.
    }
    
    // MARK: - Helper Methods
    func resignAllFirstResponders() {
        nameField?.resignFirstResponder()
        milesField?.resignFirstResponder()
        milesPerDayField?.resignFirstResponder()
    }
    
    // Create toolbar for number text fields
    func addMilesToolbar() {
        let width = view.frame.size.width
        let toolbarRect = CGRectMake(0, 0, width, 50)
        let numberToolbar = UIToolbar(frame: toolbarRect)
        numberToolbar.barStyle = .Default
        let nextButton = UIBarButtonItem(title: "Next", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(nextButtonNumberPad))
        nextButton.tintColor = UIColor.appNavyBlueColor()
        numberToolbar.items = [
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil),
            nextButton]
        numberToolbar.sizeToFit()
        milesField.inputAccessoryView = numberToolbar
    }
    
    func addMilesPerDayToolbar() {
        let width = view.frame.size.width
        let toolbarRect = CGRectMake(0, 0, width, 50)
        let numberToolbar = UIToolbar(frame: toolbarRect)
        numberToolbar.barStyle = .Default
        let saveButton = UIBarButtonItem(title: "Save", style: UIBarButtonItemStyle.Done, target: self, action: #selector(saveButtonNumberPad))
        saveButton.tintColor = UIColor.appNavyBlueColor()
        numberToolbar.items = [
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil),
            saveButton]
        numberToolbar.sizeToFit()
        milesPerDayField.inputAccessoryView = numberToolbar
    }
    
    func nextButtonNumberPad() {
        // Next button is on the miles field
        // should assign mile per day field as first responder
        milesPerDayField.becomeFirstResponder()
        scrollView.scrollTo(View: milesPerDayField)
    }
    
    func saveButtonNumberPad() {
        // Resign first responder from milesPerDayField and attempt to save
        milesPerDayField.resignFirstResponder()
        save()
    }
    
    // Check if all text fields are valid
    func textFieldsAreValid() -> Bool {
        // Check that a name was entered
        if nameField!.text == "" {
            // Request user add a name
            let title = "Car Name"
            let message = "You must enter a name for the car!"
            let ac = UIAlertController(title: title, message: message, preferredStyle: .Alert)
            let okAction = UIAlertAction(title: "OK", style: .Default, handler: { (action) -> Void in
                // Auto select car name field
                self.nameField?.becomeFirstResponder()
            })
            ac.addAction(okAction)
            presentViewController(ac, animated: true, completion: nil)
            
            return false
        }
        else if Int(milesField!.text!) == nil {
            // Request user add a miles number
            let title = "Current Mileage"
            let message = "You must enter the number of miles on the car!"
            let ac = UIAlertController(title: title, message: message, preferredStyle: .Alert)
            let okAction = UIAlertAction(title: "OK", style: .Default, handler: { (action) -> Void in
                // Auto select car miles field
                self.milesField?.becomeFirstResponder()
            })
            ac.addAction(okAction)
            presentViewController(ac, animated: true, completion: nil)
            
            return false
        }
        else if Int(milesPerDayField!.text!) == nil {
            // Request user add a miles number
            let title = "Miles Per Day"
            let message = "You must enter an estimated number of miles per day."
            let ac = UIAlertController(title: title, message: message, preferredStyle: .Alert)
            let okAction = UIAlertAction(title: "OK", style: .Default, handler: { (action) -> Void in
                // Auto select car miles per day field
                self.milesPerDayField?.becomeFirstResponder()
            })
            ac.addAction(okAction)
            presentViewController(ac, animated: true, completion: nil)
            
            return false
        }
        else if Int(milesPerDayField!.text!) < 1 {
            // Request user add a miles number
            let title = "Miles Per Day"
            let message = "You must enter an estimated mileage greater than zero."
            let ac = UIAlertController(title: title, message: message, preferredStyle: .Alert)
            let okAction = UIAlertAction(title: "OK", style: .Default, handler: { (action) -> Void in
                // Auto select car miles per day field
                self.milesPerDayField?.becomeFirstResponder()
            })
            ac.addAction(okAction)
            presentViewController(ac, animated: true, completion: nil)
            
            return false
        }
        else {
            return true
        }
    }
    
    func convertMilesToDailyValue() -> Int {
        let milesEntered = Int(milesPerDayField.text!)!
        let selectedInterval = milesTimeIntervalControl.selectedSegmentIndex
        // Segmented Control for Interval:
        // 0 = daily, 1 = weekly, 2 = yearly
        if selectedInterval == 0 {
            return milesEntered
        }
        else if selectedInterval == 1 {
            let milesEnteredAsDouble = Double(milesEntered)
            let roundedMiles = round(milesEnteredAsDouble / 7.0)
            return Int(roundedMiles)
        }
        else {
            let milesEnteredAsDouble = Double(milesEntered)
            let roundedMiles = round(milesEnteredAsDouble / 365.0)
            return Int(roundedMiles)
        }
    }
}