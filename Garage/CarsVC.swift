//
//  CarsViewController.swift
//  Garage
//
//  Created by Stuart on 4/12/16.
//  Copyright © 2016 Dauby Cafe. All rights reserved.
//

import UIKit
import CoreData

class CarsViewController: UITableViewController, NSFetchedResultsControllerDelegate, AddCarViewControllerDelegate, EditCarViewControllerDelegate {

    var managedObjectContext: NSManagedObjectContext!
    
    var addCarAttempts: Int = 0

    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Row height adjusts based on text size
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 110
        
        // Prevent tableview from showing empty cells
        tableView.tableFooterView = UIView(frame: CGRectZero)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

        // Reload tableView
        tableView.reloadData()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        // Add a car if no cars are created
        if fetchedResultsController.fetchedObjects?.count == 0 && addCarAttempts < 1 {
            addCarAttempts += 1
            presentAddCarView()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    // MARK: - Segues

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {

        if segue.identifier == "addCar" {
            let navController = segue.destinationViewController as! UINavigationController
            let controller = navController.topViewController as! AddCarViewController
            controller.delegate = self
            controller.managedObjectContext = self.managedObjectContext
        }
        else if segue.identifier == "editCar" {
            // Sender is index path of selected car
            let indexPath = sender as! NSIndexPath

            let controller = segue.destinationViewController as! EditCarViewController
            controller.delegate = self
            controller.managedObjectContext = self.managedObjectContext
            let carToEdit = fetchedResultsController.objectAtIndexPath(indexPath) as! Car
            controller.car = carToEdit
        }
        else if segue.identifier == "showWorkItems" {
            // Use sender so peek/pop works same as tap
            let cell = sender as! UITableViewCell
            let indexPath = tableView.indexPathForRowAtPoint(cell.center)!
            let car = fetchedResultsController.objectAtIndexPath(indexPath)
            
            let controller = segue.destinationViewController as! WorkItemsViewController
            controller.car = car as! Car
            controller.managedObjectContext = self.managedObjectContext
        }
        else if segue.identifier == "shortcutToCar" {
            // Sender is an int which represents the row in the fetchedResultsController
            let row = sender as! Int
            let indexPath = NSIndexPath(forRow: row, inSection: 0)
            let car = fetchedResultsController.objectAtIndexPath(indexPath)
            
            let controller = segue.destinationViewController as! WorkItemsViewController
            controller.car = car as! Car
            controller.managedObjectContext = self.managedObjectContext
        }
    }

    // MARK: - Table View

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.fetchedResultsController.sections?.count ?? 0
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionInfo = self.fetchedResultsController.sections![section]
        return sectionInfo.numberOfObjects
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("CarCell", forIndexPath: indexPath) as! CarCell
        let car = self.fetchedResultsController.objectAtIndexPath(indexPath) as! Car
        car.updateNextWorkDue()
        
        cell.updateLabels(car)
        
        return cell
    }
    
    override func tableView(tableView: UITableView, accessoryButtonTappedForRowWithIndexPath indexPath: NSIndexPath) {
        // Go to Car detail view
        performSegueWithIdentifier("editCar", sender: indexPath)
    }

    // MARK: TableView Editing
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            print("Delete action editing style still called")
        }
    }
    
    override func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        // Delete Action
        let deleteAction = UITableViewRowAction(style: .Destructive, title: "Delete" , handler: { (action:UITableViewRowAction!, indexPath:NSIndexPath!) -> Void in
            // Delete the car
            // deleteCar function verifies with user
            self.deleteCar(forRowAtIndexPath: indexPath)
        })
        
        // Edit Action
        let editAction = UITableViewRowAction(style: .Normal, title: "Edit" , handler: { (action:UITableViewRowAction!, indexPath:NSIndexPath!) -> Void in
            // Open edit car view controller
            self.performSegueWithIdentifier("editCar", sender: indexPath)
            
        })
        editAction.backgroundColor = UIColor.blueColor()

        // Update miles action
        let updateMilesAction = UITableViewRowAction(style: .Normal, title: "Update Miles", handler: { (action:UITableViewRowAction!, indexPath:NSIndexPath!) -> Void in
            // Open update car mileage alert
            let car = self.fetchedResultsController.objectAtIndexPath(indexPath) as! Car
            self.quickMilesUpdate(car)
            
        })
        updateMilesAction.backgroundColor = UIColor.statusGreenColor()
        
        return [deleteAction, editAction, updateMilesAction]
    }

    func configureCell(cell: UITableViewCell, withCar car: Car) {
        cell.textLabel!.text = car.name
        let milesString = milesFormatter.stringFromNumber(car.miles)
        cell.detailTextLabel!.text = milesString
    }
    
    private func deleteCar(forRowAtIndexPath indexPath: NSIndexPath) {
        // Create action sheet to ensure user wants to delete
        let car = fetchedResultsController.objectAtIndexPath(indexPath) as! Car
        let title = "Delete \(car.name)"
        let message = "Are you sure you want to delete this car?"
        let ac = UIAlertController(title: title, message: message, preferredStyle: .ActionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
        ac.addAction(cancelAction)
        
        let deleteAction = UIAlertAction(title: "Delete", style: .Destructive, handler: { (action) -> Void in
            // Remove car from the MOC
            let context = self.managedObjectContext!
            context.deleteObject(self.fetchedResultsController.objectAtIndexPath(indexPath) as! Car)
            // Save the MOC
            do {
                try context.save()
            } catch {
                print("Error deleting object: \(error)")
            }
        })
        ac.addAction(deleteAction)
        
        // Present the alert controller
        presentViewController(ac, animated: true, completion: nil)
    }

    // MARK: - AddCarViewController Delegate Methods
    
    func addCarViewControllerDidSave() {
        // Save the context
        CoreDataStack.save(managedObjectContext)
        
        // Dismiss the View Controller
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func addCarViewControllerDidCancel() {        
        // Dismiss View Controller
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: - EditCarViewController Delegate Methods
    
    func editCarViewControllerDidSave() {
        // Save the context
        CoreDataStack.save(managedObjectContext)
        
        // Dismiss the View Controller
        self.navigationController!.popViewControllerAnimated(true)
    }
    
    func editCarViewControllerDidCancel() {
        // Dismiss View Controller
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: - Fetched results controller

    var fetchedResultsController: NSFetchedResultsController {
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let fetchRequest = NSFetchRequest()
        // Set the entity
        let entity = NSEntityDescription.entityForName("Car", inManagedObjectContext: self.managedObjectContext!)
        fetchRequest.entity = entity
        
        // Set the batch size to a suitable number.
        fetchRequest.fetchBatchSize = 20
        
        // Sort by name in alphabetical order
        let sortDescriptor = NSSortDescriptor(key: "name", ascending: true)
        
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        // No sections and no cache being set
        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: self.managedObjectContext!, sectionNameKeyPath: nil, cacheName: nil)
        aFetchedResultsController.delegate = self
        _fetchedResultsController = aFetchedResultsController
        
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
             // Replace this implementation with code to handle the error appropriately.
             // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
             print("Unresolved error \(error)")
        }
        
        return _fetchedResultsController!
    }    
    var _fetchedResultsController: NSFetchedResultsController? = nil

    func controllerWillChangeContent(controller: NSFetchedResultsController) {
        self.tableView.beginUpdates()
    }

    func controller(controller: NSFetchedResultsController, didChangeSection sectionInfo: NSFetchedResultsSectionInfo, atIndex sectionIndex: Int, forChangeType type: NSFetchedResultsChangeType) {
        switch type {
            case .Insert:
                self.tableView.insertSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Fade)
            case .Delete:
                self.tableView.deleteSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Fade)
            default:
                return
        }
    }

    func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?) {
        switch type {
            case .Insert:
                tableView.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: .Fade)
                tableView.reloadData()
            case .Delete:
                tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: .Fade)
            case .Update:
                tableView.reloadRowsAtIndexPaths([indexPath!], withRowAnimation: .Fade)
            case .Move:
                tableView.moveRowAtIndexPath(indexPath!, toIndexPath: newIndexPath!)
                tableView.reloadData()
        }
    }

    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        self.tableView.endUpdates()
        
        createShortcutItems()
    }

    /*
     // Implementing the above methods to update the table view in response to individual changes may have performance implications if a large number of changes are made simultaneously. If this proves to be an issue, you can instead just implement controllerDidChangeContent: which notifies the delegate that all section and object changes have been processed.
     
     func controllerDidChangeContent(controller: NSFetchedResultsController) {
         // In the simplest, most efficient, case, reload the table view.
         self.tableView.reloadData()
     }
     */
    
    // MARK: - Helper Methods
    
    func quickMilesUpdate(car: Car) {
        let title = "Update \(car.name) Miles"
        let ac = UIAlertController(title: title, message: "", preferredStyle: .Alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: {
            (action) -> Void in
            self.tableView.editing = false
        })
        let updateAction = UIAlertAction(title: "Update", style: .Default, handler: { (action) -> Void in
            let mileageField = ac.textFields![0] as UITextField
            if let newMileage = Int(mileageField.text!) {
                car.miles = newMileage
            }
        })
        
        ac.addTextFieldWithConfigurationHandler({(textField) -> Void in
            textField.placeholder = "Current Mileage"
            textField.keyboardType = .DecimalPad
        })
        ac.addAction(cancelAction)
        ac.addAction(updateAction)
        
        presentViewController(ac, animated: true, completion: nil)
    }
    
    func presentAddCarView() {
        performSegueWithIdentifier("addCar", sender: nil)
    }
    
    func createShortcutItems() {
        // Creates up to 3 3D Touch shortcut items as Cars
        
        let request = NSFetchRequest(entityName: "Car")
        let sortDescriptor = NSSortDescriptor(key: "name", ascending: true)
        request.sortDescriptors = [sortDescriptor]
        var cars = [Car]()
        do {
            try cars = self.managedObjectContext.executeFetchRequest(request) as! [Car]
        } catch {
            print("No cars")
        }
        
        // Create 3 dynamic items
        var items = [UIApplicationShortcutItem]()
        let numberOfCars = cars.count - 1
        if cars.count > 0 {
            for index in 0...numberOfCars {
                // Only add up to 3 cars
                if index < 3 {
                    let carName = cars[index].name
                    let itemType = "\(index)"
                    let itemIcon = UIApplicationShortcutIcon(templateImageName: "SteeringWheel")
                    let item = UIApplicationShortcutItem(type: itemType, localizedTitle: carName, localizedSubtitle: nil, icon: itemIcon, userInfo: nil)
                    items.append(item)
                }
            }
            UIApplication.sharedApplication().shortcutItems = items
        }
        else {
            UIApplication.sharedApplication().shortcutItems = nil
        }
    }
}

