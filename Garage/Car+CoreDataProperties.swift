//
//  Car+CoreDataProperties.swift
//  Garage
//
//  Created by Stuart on 4/12/16.
//  Copyright © 2016 Dauby Cafe. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Car {
    
    // Moved to Car.swift
//    @NSManaged private var _miles: NSNumber
    
    @NSManaged var milesPerDay: NSNumber
    @NSManaged var name: String
    @NSManaged var workItems: NSSet
    
    @NSManaged var dateOfLastMilesUpdate: NSDate

}