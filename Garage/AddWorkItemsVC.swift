//
//  AddWorkItemsViewController.swift
//  Garage
//
//  Created by Stuart on 4/17/16.
//  Copyright © 2016 Dauby Cafe. All rights reserved.
//

import UIKit
import CoreData

protocol AddWorkItemsViewControllerDelegate {
    func addWorkItemsViewControllerDidSave()
}

class AddWorkItemsViewController: AddViewController, UITableViewDelegate, UITableViewDataSource, CreateCustomWorkItemVCDelegate {
    
    // MARK: - Properties & Outlets
    @IBOutlet weak var tableView: UITableView!

    // Array initialized as empty but is replaced with contents from NSUserDefaults
    // during cellForRowAtIndexPath call
    var defaultItems: [AnyObject] = Array()
    
    var car: Car!
    var managedObjectContext: NSManagedObjectContext!
    var delegate: AddWorkItemsViewControllerDelegate!
    
    var indexPathsForAlreadySelectedItems = [NSIndexPath]()
    
    @IBAction func save(sender: AnyObject) {
        
        createWorkItemsToSave()
        
        delegate.addWorkItemsViewControllerDidSave()
    }
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "createCustomItem" {
            // Deselect any selected items
            if let selectedRows = tableView.indexPathsForSelectedRows {
                for indexPath in selectedRows {
                    tableView.deselectRowAtIndexPath(indexPath, animated: true)
                    tableView.cellForRowAtIndexPath(indexPath)?.accessoryType = .None
                }
            }
            
            // Custom item view controller
            let controller = segue.destinationViewController as! CreateCustomWorkItemViewController
            controller.delegate = self 
        }
    }
    
    // MARK: - TableView Methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Extra row to account for custom item link
        
        let storedDefaultItems = NSUserDefaults.standardUserDefaults().arrayForKey("defaultItems")
        if let storedDefaultItems = storedDefaultItems {
            defaultItems = storedDefaultItems
        }
        
        return defaultItems.count + 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        // Create set of workItem names that the car uses
        let carItemNames = car.workItems.valueForKey("name")
        
        let row = indexPath.row
        if row < (tableView.numberOfRowsInSection(0) - 1) {
            let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
            
            // Update Labels
            if let defaultItem = defaultItems[row] as? NSData {
                let item = NSKeyedUnarchiver.unarchiveObjectWithData(defaultItem) as! DefaultWorkItem
                let defaultItemName = item.name
                
                if carItemNames.containsObject(defaultItemName) {
                    cell.accessoryType = .Checkmark
                    cell.tintColor = UIColor.grayColor()
                }
                configureCell(cell, withName: item.name)
            }
            
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCellWithIdentifier("LastCell", forIndexPath: indexPath)
            return cell
        }
        
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete from defaults
            defaultItems.removeAtIndex(indexPath.row)
            NSUserDefaults.standardUserDefaults().setObject(defaultItems, forKey: "defaultItems")
            // Delete from table
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        
        let row = indexPath.row
        if row < (tableView.numberOfRowsInSection(0) - 1) {
            // Show checkmark to visualize selected cells
            cell?.accessoryType = .Checkmark
        }
        else {
            // Send to create custom item view
            self.tableView.deselectRowAtIndexPath(indexPath, animated: false)
            performSegueWithIdentifier("createCustomItem", sender: nil)
        }
        
    }
    
    func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        // If item is already tracked, don't select the cell
        
        let row = indexPath.row
        // Create set of workItem names that the car uses
        let carItemNames = car.workItems.valueForKey("name")
        
        // Don't allow checking last row (create new item row)
        if row < (tableView.numberOfRowsInSection(0) - 1) {
            if let defaultItem = defaultItems[row] as? NSData {
                let item = NSKeyedUnarchiver.unarchiveObjectWithData(defaultItem) as! DefaultWorkItem
                let defaultItemName = item.name
                
                if carItemNames.containsObject(defaultItemName) {
                    return nil
                }
            }
        }
        return indexPath
    }
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        // Remove checkmark to visualize deselected cells
        cell?.accessoryType = .None
    }
    
    func configureCell(cell: UITableViewCell, withName name: String) {
        cell.textLabel!.text = name
    }
    
    // MARK: - CreateCustomWorkItemVC Delegate Methods
    func createCustomWorkItemVCDidSave() {
        tableView.reloadData()
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func createCustomWorkItemVCDidCancel() {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: - Helper Methods
    
    func createWorkItemsToSave() {
        // If rows are selected
        if let indexPaths = tableView.indexPathsForSelectedRows {
            // Iterate through selected rows
            for index in indexPaths {
                // Unwrap the default item at that row
                if let defaultItem = defaultItems[index.row] as? NSData {
                    // Unarchive the item
                    let item = NSKeyedUnarchiver.unarchiveObjectWithData(defaultItem) as! DefaultWorkItem
                    
                    // Save item as a WorkItem and add it to its car
                    let entity = NSEntityDescription.entityForName("WorkItem", inManagedObjectContext: managedObjectContext)
                    let newItem = NSManagedObject.init(entity: entity!, insertIntoManagedObjectContext: managedObjectContext) as! WorkItem
                    newItem.name = item.name
                    newItem.trackedByMiles = item.trackedByMiles
                    newItem.trackedByMonths = item.trackedByMonths
                    newItem.mileInterval = item.mileInterval
                    newItem.monthInterval = item.monthInterval
                    newItem.car = self.car
                    
                    // Add initial entry w/ todays date and mileage
                    addFirstEntry(newItem)
                }
            }
        }
    }
    
    func addFirstEntry(workItem: WorkItem) {
        
        // Create new entry in context
        let newEntry = NSEntityDescription.insertNewObjectForEntityForName("WorkItemEntry", inManagedObjectContext: managedObjectContext) as! WorkItemEntry
        
        let date = NSDate()
        let miles = workItem.car!.miles
        
        newEntry.dateCompleted = date
        newEntry.milesCompleted = miles
        newEntry.notes = "Default entry to begin tracking \(workItem.name) for \(workItem.car!.name). Update the entry to the miles (odometer reading) and date of the last time you completed this item for your car."
        newEntry.workItem = workItem
    }
    
    
}
