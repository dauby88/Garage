//
//  Car.swift
//  Garage
//
//  Created by Stuart on 4/12/16.
//  Copyright © 2016 Dauby Cafe. All rights reserved.
//

import Foundation
import CoreData


class Car: NSManagedObject {
    
    // Car miles value presented to user
    // Can be actual or estimate
    @NSManaged var rawMiles: NSNumber
    
    var estimatedMiles: NSNumber {
        get {
            return updateEstimatedMiles()
        }
    }
    
    var miles: NSNumber {
        get {
            if (rawMiles.integerValue + 1000) > estimatedMiles.integerValue {
                // Use mile value
                return self.rawMiles.integerValue
            }
            else {
                // Use estimated value
                return self.estimatedMiles.integerValue
            }
        }
        set {
            self.rawMiles = newValue
            self.dateOfLastMilesUpdate = NSDate()
        }
    }
    
    var nextWorkDue: WorkItem?
    
    // Calculate the next work item that is due
    
    func updateNextWorkDue() {
        
        // This function sets the nextWorkDue item and the miles and days until due variables
        // It's important that the last completed entry is up to date when this function is called.

        // Get the last work item entry for each work item
        var workItemsDue = [WorkItem]()
        for object in self.workItems {
            // Cast item as WorkItem
            let item = object as! WorkItem
            item.updateNextDue()
            
            // Append to array of WorkItems if it has an estimated due date
            if item.approxDaysUntilDue != nil {
                workItemsDue.append(item)
            }
        }

        workItemsDue.sortInPlace( { $0.approxDaysUntilDue < $1.approxDaysUntilDue } )
        
        if let earliestItem = workItemsDue.first {
            nextWorkDue = earliestItem
        } else {
            nextWorkDue = nil
        }
        
        // Reschedule notifications for the car
        NotificationStack.scheduleLocalNotifications(ForCar: self)
    }
    
    private func updateEstimatedMiles() -> Int {
        let today = NSDate()
        let lastUpdate = self.dateOfLastMilesUpdate
        let dailyMiles = self.milesPerDay.integerValue
        
        let components = NSCalendar.currentCalendar().components(.Day, fromDate: lastUpdate, toDate: today, options: .WrapComponents)
        let daysSinceUpdate = components.day
        
        let unroundedEstMiles = Double(daysSinceUpdate * dailyMiles)
        let roundedEstMiles = round(unroundedEstMiles / 500) * 500
        return Int(roundedEstMiles) + rawMiles.integerValue
    }
}
