//
//  WorkItemCell.swift
//  Garage
//
//  Created by Stuart on 4/23/16.
//  Copyright © 2016 Dauby Cafe. All rights reserved.
//

import UIKit

class WorkItemCell: UITableViewCell {
    
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var whenDueLabel: UILabel!
    @IBOutlet var dueInDays: UILabel!
    @IBOutlet var statusBar: UIView!
    
    // Custom highlighting so the status bar isn't covered on selection
    override func setHighlighted(highlighted: Bool, animated: Bool) {        
        if (highlighted) {
            self.contentView.superview?.backgroundColor = UIColor.reallyLightGrayColor()
        }
        else {
            self.contentView.superview?.backgroundColor = UIColor.clearColor()
        }
    }
    
    func updateLabels(item: WorkItem) {
        
        let title2Font = UIFont.preferredFontForTextStyle(UIFontTextStyleTitle2)
        let calloutFont = UIFont.preferredFontForTextStyle(UIFontTextStyleCallout)
        
        // Set name of WorkItem
        nameLabel.text = item.name
        nameLabel.font = title2Font
        
        // Set whenDueLabel string
        whenDueLabel.font = calloutFont
        updateWhenDueLabel(item)
        
        // Set dueInDays string and color of cell status bar
        dueInDays.font = calloutFont
        if let daysUntildue = item.approxDaysUntilDue {
            statusBar.backgroundColor = statusBarColor(daysUntildue)
            
            if daysUntildue > 0 {
                dueInDays.textColor = UIColor.blackColor()
                dueInDays.text = "Approx. \(daysUntildue.dayWkMonString)"
            }
            else {
                // Item is due
                dueInDays.textColor = UIColor.statusRedColor()
                dueInDays.text = "Overdue!!!"
            }
        } else {
            dueInDays.text = ""
            statusBar.backgroundColor = UIColor.lightGrayColor()
        }
    }
    
    
    // MARK: - Helper Methods
    
    private func updateWhenDueLabel(item: WorkItem) {
        
        if item.lastCompletedEntry == nil {
            whenDueLabel.text = "No History"
            whenDueLabel.textColor = UIColor.grayColor()
        }
        else {
            if item.trackedByMiles.boolValue && item.trackedByMonths.boolValue {
                if let milesDue = item.milesDue, dueDate = item.dueDate {
                    let milesText = milesDue.milesString
                    let dueDateText = dateFormatter.stringFromDate(dueDate)
                    whenDueLabel.textColor = UIColor.blackColor()
                    whenDueLabel.text = "Due at \(milesText) or by \(dueDateText)"
                }
            }
            else if item.trackedByMiles.boolValue {
                if let milesDue = item.milesDue {
                    let milesText = milesDue.milesString
                    whenDueLabel.textColor = UIColor.blackColor()
                    whenDueLabel.text = "Due at \(milesText)"
                }
            }
            else if item.trackedByMonths.boolValue {
                if let dueDate = item.dueDate {
                    let dueDateText = dateFormatter.stringFromDate(dueDate)
                    whenDueLabel.textColor = UIColor.blackColor()
                    whenDueLabel.text = "Due by \(dueDateText)"
                }
            }
            else {
                whenDueLabel.textColor = UIColor.grayColor()
                whenDueLabel.text = "No Due Date"
            }
        }
    }
    
    private func statusBarColor(days: Int) -> UIColor {
        if days < 1 {
            return UIColor.statusRedColor()
        }
        else if days < 60 {
            return UIColor.statusYellowColor()
        }
        else {
            return UIColor.statusGreenColor()
        }
    }

}