//
//  Int+DayWeekMonthString.swift
//  Garage
//
//  Created by Stuart on 5/4/16.
//  Copyright © 2016 Dauby Cafe. All rights reserved.
//

import Foundation

extension Int {
    var dayWkMonString: String {
        let daysAsDouble = Double(self)
        let days = Int(daysAsDouble)
        let weeks = Int(round(daysAsDouble/7))
        let months = Int(round(daysAsDouble/30))
        let years = Int(round(daysAsDouble/365))
        
        switch days {
        case _ where days == 1 :
            return "1 day"
        case _ where days < 7 :
            return "\(days) days"
        case _ where weeks == 1:
            return "1 week"
        case _ where weeks < 4:
            return "\(weeks) weeks"
        case _ where months == 1:
            return "1 month"
        case _ where months < 12:
            return "\(months) months"
        case _ where years == 1:
            return "1 year"
        default:
            return "2+ years"
        }
    }
    
    var milesString: String {
        if let formattedString = milesFormatter.stringFromNumber(self) {
            return "\(formattedString) miles"
        } else {
            return "xxx,xxx miles"
        }
    }
    
    var estMilesString: String {
        if let formattedString = milesFormatter.stringFromNumber(self) {
            return "\(formattedString) est. miles"
        } else {
            return "xxx,xxx miles"
        }
    }
}