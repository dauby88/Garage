//
//  WorkItemEntryCell.swift
//  Garage
//
//  Created by Stuart on 5/4/16.
//  Copyright © 2016 Dauby Cafe. All rights reserved.
//

import UIKit

class WorkItemEntryCell: UITableViewCell {
    
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var daysAgoLabel: UILabel!
    @IBOutlet var milesLabel: UILabel!
    @IBOutlet var milesAgoLabel: UILabel!
    @IBOutlet var notesLabel: UILabel!
    
    func updateLabels(entry: WorkItemEntry) {

        let bodyFont = UIFont.preferredFontForTextStyle(UIFontTextStyleBody)
        let calloutFont = UIFont.preferredFontForTextStyle(UIFontTextStyleCallout)
        let footnoteFont = UIFont.preferredFontForTextStyle(UIFontTextStyleFootnote)
        
        // Set dateLabel
        let dateString = dateFormatter.stringFromDate(entry.dateCompleted)
        dateLabel.text = dateString
        dateLabel.font = bodyFont

        // Set milesLabel
        milesLabel.text = "\(entry.milesCompleted.milesString)"
        milesLabel.font = bodyFont
        
        // Set milesAgoLabel
        let carMilesInteger = entry.workItem.car!.miles.integerValue
        let milesCompletedInteger = entry.milesCompleted.integerValue
        let milesAgo: NSNumber = carMilesInteger - milesCompletedInteger
        milesAgoLabel.text = "\(milesAgo.milesString) ago"
        milesAgoLabel.font = calloutFont
        
        // Set daysAgoLabel
        let today = NSDate()
        let toDate = entry.dateCompleted
        let components = NSCalendar.currentCalendar().components(.Day, fromDate: toDate, toDate: today, options: .MatchStrictly)
        let daysAgo = components.day
        let timeAgo = daysAgo.dayWkMonString
        daysAgoLabel.text = "\(timeAgo) ago"
        daysAgoLabel.font = calloutFont
        
        // Set notesLabel
        let notes = entry.notes
        notesLabel.text = notes
        notesLabel.font = footnoteFont
    }
}