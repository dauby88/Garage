//
//  AddWorkItemEntryViewController.swift
//  Garage
//
//  Created by Stuart on 4/21/16.
//  Copyright © 2016 Dauby Cafe. All rights reserved.
//

import UIKit
import CoreData

class AddWorkItemEntryViewController: AddViewController {
    
    // MARK: Properties and Outlets
    
    var managedObjectContext: NSManagedObjectContext!
    var workItem: WorkItem!
    @IBOutlet var scrollView: UIScrollView!
    
    @IBOutlet var workItemNameLabel: UILabel! {
        didSet {
            workItemNameLabel.text = workItem.name
        }
    }
    
    @IBOutlet var dateCompletedField: UITextField! {
        didSet {
            // Default text is today's date
            let dateText = dateFormatter.stringFromDate(NSDate())
            dateCompletedField?.text = dateText
            
            // Remove the blinking cursor
            dateCompletedField?.tintColor = UIColor.clearColor()
            
            // Use date picker as input field for date completed
            let datePicker = UIDatePicker()
            datePicker.datePickerMode = .Date
            datePicker.maximumDate = NSDate()
            datePicker.addTarget(self, action: #selector(updateTextField), forControlEvents: .ValueChanged)
            dateCompletedField?.inputView = datePicker
        }
    }
    @IBOutlet var milesCompletedField: UITextField! {
        didSet {
            // Default miles is the current car mileage
            let field = milesCompletedField
            field?.text = String(workItem.car!.miles)
        }
    }
    
    
    @IBOutlet var stackViewWidthConstraint: NSLayoutConstraint! {
        didSet {
            let screenRect = UIScreen.mainScreen().bounds
            let screenWidth = screenRect.width
            stackViewWidthConstraint.constant = screenWidth - 16
        }
    }
    
    @IBOutlet var notesField: UITextView! {
        didSet {
            let field = notesField!
            field.layer.cornerRadius = 5.0
            field.layer.borderWidth = 0.5
            field.layer.borderColor = UIColor.borderColor().CGColor
        }
    }

    
    @IBAction func cancel(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    @IBAction func done(sender: AnyObject) {
        // Check that a date was entered
        let itemName = workItem.name
        
        if dateFormatter.dateFromString(dateCompletedField!.text!) == nil {
            // Request user add a date
            let title = "Entry Date"
            let message = "Something is wrong with the date field!"
            let ac = UIAlertController(title: title, message: message, preferredStyle: .Alert)
            let okAction = UIAlertAction(title: "OK", style: .Default, handler: { (action) -> Void in
                // Auto select date field
                self.dateCompletedField?.becomeFirstResponder()
            })
            ac.addAction(okAction)
            
            presentViewController(ac, animated: true, completion: nil)
        }
        else if Int(milesCompletedField!.text!) == nil {
            // Request user add a miles number
            let title = "Miles Completed"
            let message = "You must enter the number of miles when the \(itemName) was completed"
            let ac = UIAlertController(title: title, message: message, preferredStyle: .Alert)
            let okAction = UIAlertAction(title: "OK", style: .Default, handler: { (action) -> Void in
                // Auto select car miles field
                self.milesCompletedField?.becomeFirstResponder()
            })
            ac.addAction(okAction)
            presentViewController(ac, animated: true, completion: nil)
        }
        
        // Update car miles if selected
        if willUpdateMilesSwitch.on {
            if let miles = Int((milesCompletedField?.text)!) {
                // Only update if miles are higher than current car mileage
                if miles > workItem.car!.miles.integerValue {
                    workItem.car!.miles = miles
                }
            }
        }
        
        // Add the entry to the work item and dismiss view
        addEntry()
        // Update the last completed entry and next due stats
        workItem.updateNextDue()
        let car = workItem.car!
        car.updateNextWorkDue()
        dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Create toolbars for number and date picker fields
        addMilesCompletedToolbar()
        addDatePickerToolbar()
        
        // Add target for switch
        willUpdateMilesSwitch.addTarget(self, action: #selector(updateMilesSwitchChanged), forControlEvents: .ValueChanged)
        
        // Make first text field first responder right away
        milesCompletedField?.becomeFirstResponder()
    }
    
    override func viewWillDisappear(animated: Bool) {
        dateCompletedField?.resignFirstResponder()
        milesCompletedField?.resignFirstResponder()
        notesField?.resignFirstResponder()
        
        super.viewWillDisappear(animated)
    }
    
    // MARK: - Update Miles Switch
    @IBOutlet var willUpdateMilesSwitch: UISwitch!
    
    @IBOutlet var updateMilesLabel: UILabel! {
        didSet {
            updateMilesLabel.hidden = true
        }
    }
    
    func updateMilesSwitchChanged(theSwitch: UISwitch) {
        let switchOn = theSwitch.on
        
        if switchOn {
            UIView.transitionWithView(updateMilesLabel, duration: 0.25, options: .TransitionCrossDissolve, animations: { self.updateMilesLabel.hidden = false }, completion: nil)
        } else {
            UIView.transitionWithView(updateMilesLabel, duration: 0.25, options: .TransitionCrossDissolve, animations: { self.updateMilesLabel.hidden = true }, completion: nil)
        }
    }
    
    // MARK: - Helper Methods
    
    func addEntry() {
        
        // Create new entry in context
        let newEntry = NSEntityDescription.insertNewObjectForEntityForName("WorkItemEntry", inManagedObjectContext: managedObjectContext) as! WorkItemEntry
        
        let date = dateFormatter.dateFromString(dateCompletedField!.text!)!
        newEntry.dateCompleted = date
        newEntry.milesCompleted = Int((milesCompletedField?.text!)!)!
        newEntry.notes = notesField!.text!
        newEntry.workItem = self.workItem
        
        // Save the context
        do {
            try managedObjectContext!.save()
        } catch {
            fatalError("Failure to save context: \(error)")
        }
    }
    
    func updateTextField(sender: UIDatePicker) {
        let date = sender.date
        self.dateCompletedField!.text = dateFormatter.stringFromDate(date)
    }
    
    // Create toolbar for date picker
    func addDatePickerToolbar() {
        let width = view.frame.size.width
        let toolbarRect = CGRectMake(0, 0, width, 50)
        let toolbar = UIToolbar(frame: toolbarRect)
        toolbar.barStyle = .Default
        toolbar.items = [
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil),
            UIBarButtonItem(title: "Next", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(moveToNotesField))]
        toolbar.sizeToFit()
        dateCompletedField?.inputAccessoryView = toolbar
    }
    
    // Create toolbar for date picker
    func addMilesCompletedToolbar() {
        let width = view.frame.size.width
        let toolbarRect = CGRectMake(0, 0, width, 50)
        let toolbar = UIToolbar(frame: toolbarRect)
        toolbar.barStyle = .Default
        toolbar.items = [
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil),
            UIBarButtonItem(title: "Next", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(moveToDateCompletedField))]
        toolbar.sizeToFit()
        milesCompletedField?.inputAccessoryView = toolbar
    }
    
    func moveToDateCompletedField() {
        dateCompletedField?.becomeFirstResponder()
        scrollView.scrollTo(View: dateCompletedField)
    }
    
    func moveToNotesField() {
        notesField.becomeFirstResponder()
        scrollView.scrollTo(View: notesField)
    }

    
    @IBAction func dismissKeyboard(sender: AnyObject) {
        milesCompletedField?.resignFirstResponder()
        dateCompletedField?.resignFirstResponder()
        notesField?.resignFirstResponder()
    }
}