//
//  WorkItem+CoreDataProperties.swift
//  Garage
//
//  Created by Stuart on 4/20/16.
//  Copyright © 2016 Dauby Cafe. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension WorkItem {

    @NSManaged var mileInterval: NSNumber?
    @NSManaged var monthInterval: NSNumber?
    @NSManaged var name: String
    @NSManaged var trackedByMiles: NSNumber
    @NSManaged var trackedByMonths: NSNumber
    @NSManaged var car: Car?
    @NSManaged var workItemEntries: NSSet?

}
