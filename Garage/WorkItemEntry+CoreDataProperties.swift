//
//  WorkItemEntry+CoreDataProperties.swift
//  Garage
//
//  Created by Stuart on 4/20/16.
//  Copyright © 2016 Dauby Cafe. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension WorkItemEntry {

    @NSManaged var dateCompleted: NSDate
    @NSManaged var milesCompleted: NSNumber
    @NSManaged var notes: String
    @NSManaged var workItem: WorkItem

}
