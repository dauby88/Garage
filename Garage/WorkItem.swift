//
//  WorkItem.swift
//  Garage
//
//  Created by Stuart on 4/17/16.
//  Copyright © 2016 Dauby Cafe. All rights reserved.
//

import Foundation
import CoreData


class WorkItem: NSManagedObject {

    // MARK: - Properties
    
    var lastCompletedEntry: WorkItemEntry?
    
    var milesUntilNextDue: Int?
    var milesDue: Int?
    var daysUntilMileLimit: Int? // Days until mile limit; based on Car's miles per day estimate
    
    var dueDate: NSDate?
    var daysUntilDueDate: Int? // Exact number of days until the due date
    
    var approxDaysUntilDue: Int? // Estimated days until miles run out or due date comes: whichever is sooner

    
    // MARK: - Class Methods
    
    // Update next due variables
    // What work item is due next, in how many miles and days
    func updateNextDue() {
        // Define the last completed entry
        updateLastCompletedEntry()
        
        // If there are no entries, every "next" variable is nil
        if lastCompletedEntry == nil {
            nilAllNextDueStats()
        } else {
            let currentMileage = self.car!.miles.integerValue
            
            // If tracking by miles, calculate mile stats
            if trackedByMiles.boolValue {
                // Calculate miles until next due
                milesDue = mileInterval!.integerValue + lastCompletedEntry!.milesCompleted.integerValue
                
                milesUntilNextDue = milesDue! - currentMileage
                daysUntilMileLimit = milesUntilNextDue! / car!.milesPerDay.integerValue
            }
            else {
                milesUntilNextDue = nil
                milesDue = nil
            }
            
            // If tracking by months, calculate date stats
            if trackedByMonths.boolValue {
                // Calculate days until next due and due date
                let dateCompleted = lastCompletedEntry!.dateCompleted
                let interval = self.monthInterval!.integerValue
                var components = NSDateComponents()
                components.setValue(interval, forComponent: NSCalendarUnit.Month)
                
                dueDate = NSCalendar.currentCalendar().dateByAddingComponents(components, toDate: dateCompleted, options: NSCalendarOptions(rawValue: 0))
                let today = NSDate()
                components = NSCalendar.currentCalendar().components(.Day, fromDate: today, toDate: dueDate!, options: NSCalendarOptions(rawValue: 0))
                daysUntilDueDate = components.day + 1 // Due date doesn't count current day
            }
            else {
                daysUntilDueDate = nil
                dueDate = nil
            }
            
            calculateApproxDaysUntilDue()
        }
        
        
    }
    
    // MARK: - Helper functions
    
    // Set last completed entry
    private func updateLastCompletedEntry() {
        // Sort list of entries and get the first one
        let sortDescriptor = NSSortDescriptor.init(key: "dateCompleted", ascending: false)
        let sortedEntries = workItemEntries?.sortedArrayUsingDescriptors([sortDescriptor]) as! [WorkItemEntry]
        
        if let lastEntry = sortedEntries.first {
            lastCompletedEntry = lastEntry
        } else {
            lastCompletedEntry = nil
        }
    }
    
    private func calculateApproxDaysUntilDue() {
        if daysUntilDueDate != nil && daysUntilMileLimit != nil {
            // Find the smaller number and set it as approx days until due
            approxDaysUntilDue = (daysUntilDueDate < daysUntilMileLimit) ? daysUntilDueDate : daysUntilMileLimit
        }
        else if daysUntilDueDate != nil {
            approxDaysUntilDue = daysUntilDueDate
        }
        else if daysUntilMileLimit != nil {
            approxDaysUntilDue = daysUntilMileLimit
        }
        else {
            approxDaysUntilDue = nil
        }
    }

    
    private func nilAllNextDueStats() {
        milesUntilNextDue = nil
        milesDue = nil
        daysUntilDueDate = nil
        dueDate = nil
        approxDaysUntilDue = nil
    }
}
