//
//  ScrollView+Scroll.swift
//  Garage
//
//  Created by Stuart on 5/30/16.
//  Copyright © 2016 Dauby Cafe. All rights reserved.
//

import UIKit

extension UIScrollView {
    
    func scrollTo(View view: UIView) {
        
        let viewOrigin = view.frame.origin
        let viewOriginInWindow = view.superview!.convertPoint(viewOrigin, toView: nil)
        
        let screenRect = UIScreen.mainScreen().bounds
        let screenHeight = screenRect.size.height
        
        let viewDistanceFromBottom = screenHeight - viewOriginInWindow.y

        if viewDistanceFromBottom < 260 {
            var point = viewOriginInWindow
            point.y -= 150
            point.x = 0
            self.setContentOffset(point, animated: true)
        }
    }
}
