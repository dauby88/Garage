//
//  NoPasteUITextField.swift
//  Garage
//
//  Created by Stuart on 5/8/16.
//  Copyright © 2016 Dauby Cafe. All rights reserved.
//

import UIKit

class NoPasteUITextField: UITextField {
    
    // Disallow paste on the text field
    override func canPerformAction(action: Selector, withSender sender: AnyObject?) -> Bool {
        if action == #selector(paste) {
            return false
        }
        return super.canPerformAction(action, withSender: sender)
    }
}
