//
//  AddViewController.swift
//  Garage
//
//  Created by Stuart on 5/8/16.
//  Copyright © 2016 Dauby Cafe. All rights reserved.
//

import UIKit

class AddViewController: UIViewController {
    // Class for the add view controllers which do not inheirit a navigation controller
    // but have a nav bar for UI consistency.
    // The viewDidLoad call makes the status bar white
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }
    
}
