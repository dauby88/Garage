//
//  GlobalFormatters.swift
//  CarTracker
//
//  Created by Stuart on 4/3/16.
//  Copyright © 2016 Dauby Cafe. All rights reserved.
//

import UIKit

let dateFormatter: NSDateFormatter = {
    let formatter = NSDateFormatter()
    formatter.dateStyle = .MediumStyle
    formatter.timeStyle = .NoStyle
    return formatter
} ()

let milesFormatter: NSNumberFormatter = {
    let formatter = NSNumberFormatter()
    formatter.numberStyle = .DecimalStyle
    formatter.maximumFractionDigits = 0
    return formatter
} ()


