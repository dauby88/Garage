//
//  EditWorkItemEntryVC.swift
//  Garage
//
//  Created by Stuart on 5/9/16.
//  Copyright © 2016 Dauby Cafe. All rights reserved.
//

import UIKit
import CoreData

class EditWorkItemEntryViewController: AddViewController {
    // MARK: Properties and Outlets
    
    var managedObjectContext: NSManagedObjectContext!
    var workItemEntry: WorkItemEntry!
    
    @IBOutlet var scrollView: UIScrollView!
    
    @IBOutlet var workItemNameLabel: UILabel! {
        didSet {
            workItemNameLabel.text = workItemEntry.workItem.name
        }
    }
    
    @IBOutlet var dateCompletedField: UITextField! {
        didSet {
            // Default text is today's date
            let date = workItemEntry.dateCompleted
            let dateText = dateFormatter.stringFromDate(date)
            dateCompletedField?.text = dateText
            
            // Remove the blinking cursor
            dateCompletedField?.tintColor = UIColor.clearColor()
            
            // Use date picker as input field for date completed
            let datePicker = UIDatePicker()
            datePicker.datePickerMode = .Date
            datePicker.maximumDate = NSDate()
            datePicker.date = workItemEntry.dateCompleted
            datePicker.addTarget(self, action: #selector(updateTextField), forControlEvents: .ValueChanged)
            dateCompletedField?.inputView = datePicker
        }
    }
    @IBOutlet var milesCompletedField: UITextField! {
        didSet {
            // Default miles is the current car mileage
            milesCompletedField?.text = String(workItemEntry.milesCompleted)
        }
    }
    
    @IBOutlet var notesFieldWidthConstraint: NSLayoutConstraint! {
        didSet {
            let screenRect = UIScreen.mainScreen().bounds
            let screenWidth = screenRect.width
            notesFieldWidthConstraint.constant = screenWidth - 16
        }
    }
    @IBOutlet var notesField: UITextView! {
        didSet {
            let field = notesField!
            
            field.layer.cornerRadius = 5.0
            field.layer.borderWidth = 0.5
            field.layer.borderColor = UIColor.borderColor().CGColor
            
            field.text = workItemEntry.notes
        }
    }
    
    func save() {
        // Save the entry
    
        if dateFormatter.dateFromString(dateCompletedField!.text!) == nil {
            // Don't save
            return
        }
        else if Int(milesCompletedField!.text!) == nil {
            // Don't save
            return
        }
        
        // Update the entry and save the context
        saveEntry()
        // Update the last completed entry and next due stats
        workItemEntry.workItem.updateNextDue()
        let car = workItemEntry.workItem.car!
        car.updateNextWorkDue()

        resignAllFirstResponders()
    }
    
    // MARK: View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Create toolbars for number and date picker fields
        addMilesCompletedToolbar()
        addDatePickerToolbar()
    }
    
    override func viewDidDisappear(animated: Bool) {
        save()
        resignAllFirstResponders()
        
        super.viewDidDisappear(animated)
    }
    
    // MARK: Helper Methods
    
    func saveEntry() {
        
        let date = dateFormatter.dateFromString(dateCompletedField!.text!)!
        workItemEntry.dateCompleted = date
        workItemEntry.milesCompleted = Int((milesCompletedField?.text!)!)!
        workItemEntry.notes = notesField!.text!
        
        // Save the context
        CoreDataStack.save(managedObjectContext)
    }
    
    func updateTextField(sender: UIDatePicker) {
        let date = sender.date
        self.dateCompletedField!.text = dateFormatter.stringFromDate(date)
    }
    
    // Create toolbar for date picker
    func addDatePickerToolbar() {
        let width = view.frame.size.width
        let toolbarRect = CGRectMake(0, 0, width, 50)
        let toolbar = UIToolbar(frame: toolbarRect)
        toolbar.barStyle = .Default
        toolbar.items = [
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil),
            UIBarButtonItem(title: "Next", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(moveToNotesField))]
        toolbar.sizeToFit()
        dateCompletedField?.inputAccessoryView = toolbar
    }
    
    // Create toolbar for date picker
    func addMilesCompletedToolbar() {
        let width = view.frame.size.width
        let toolbarRect = CGRectMake(0, 0, width, 50)
        let toolbar = UIToolbar(frame: toolbarRect)
        toolbar.barStyle = .Default
        toolbar.items = [
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil),
            UIBarButtonItem(title: "Next", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(moveToDateCompletedField))]
        toolbar.sizeToFit()
        milesCompletedField?.inputAccessoryView = toolbar
    }
    
    func moveToDateCompletedField() {
        dateCompletedField.becomeFirstResponder()
        scrollView.scrollTo(View: dateCompletedField)
    }
    func moveToNotesField() {
        notesField.becomeFirstResponder()
        scrollView.scrollTo(View: notesField)
    }
    
    @IBAction func dismissKeyboard(sender: AnyObject) {
        resignAllFirstResponders()
    }
    func resignAllFirstResponders() {
        milesCompletedField?.resignFirstResponder()
        dateCompletedField?.resignFirstResponder()
        notesField?.resignFirstResponder()
    }
}
