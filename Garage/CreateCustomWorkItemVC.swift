//
//  CreateCustomWorkItemVC.swift
//  Garage
//
//  Created by Stuart on 5/10/16.
//  Copyright © 2016 Dauby Cafe. All rights reserved.
//

import UIKit

protocol CreateCustomWorkItemVCDelegate {
    func createCustomWorkItemVCDidSave()
    func createCustomWorkItemVCDidCancel()
}

class CreateCustomWorkItemViewController: AddViewController {
    
    // MARK: - Properties & Outlets
    
    var delegate: CreateCustomWorkItemVCDelegate!
    
    @IBOutlet var nameField: UITextField!
    @IBOutlet var trackedByMilesSwitch: UISwitch!
    @IBOutlet var mileIntervalField: UITextField!
    @IBOutlet var trackedByMonthsSwitch: UISwitch!
    @IBOutlet var monthIntervalField: UITextField!
    
    @IBAction func cancel(sender: AnyObject) {
        resignAllResponders()
        // Call delegate method to cancel
        delegate.createCustomWorkItemVCDidCancel()
    }
    @IBAction func done(sender: AnyObject) {
        if textFieldsAreValid() {
            resignAllResponders()
            // Save inputs to NSUserDefaults
            saveToNSUserDefaults()
            // Call delegate method
            delegate.createCustomWorkItemVCDidSave()
        } else {
            return
        }
    }
    @IBAction func dismissKeyboard(sender: AnyObject) {
        resignAllResponders()
    }
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameField.becomeFirstResponder()
        
        // Add targets for switches
        trackedByMilesSwitch.addTarget(self, action: #selector(milesSwitchChanged), forControlEvents: .ValueChanged)
        trackedByMonthsSwitch.addTarget(self, action: #selector(monthsSwitchChanged), forControlEvents: .ValueChanged)
        
        // Add toolbars to number pads
        addMilesToolbars()
    }
    
    // MARK: - Switch Logic
    
    // Switches react to changes and immediately update the work item
    
    func milesSwitchChanged(theSwitch: UISwitch) {
        let switchOn = theSwitch.on
        
        if switchOn {
            mileIntervalField.enabled = true
            mileIntervalField.becomeFirstResponder()
        } else {
            mileIntervalField.text = nil
            mileIntervalField.enabled = false
        }
    }
    
    func monthsSwitchChanged(theSwitch: UISwitch) {
        let switchOn = theSwitch.on
        
        if switchOn {
            monthIntervalField.enabled = true
            monthIntervalField.becomeFirstResponder()
        } else {
            monthIntervalField.text = nil
            monthIntervalField.enabled = false
        }
    }
    
    // MARK: - Helper Methods
    
    func saveToNSUserDefaults() {
        let name = nameField.text!
        var tbMiles: Bool
        var mileInterval: Int?
        var tbMonths: Bool
        var monthInterval: Int?
        
        // Get current array of default items
        var defaultItems = NSUserDefaults.standardUserDefaults().arrayForKey("defaultItems")
        // Encode new object
        
        if trackedByMilesSwitch.on {
            tbMiles = true
            mileInterval = Int(mileIntervalField.text!)
        } else {
            tbMiles = false
            mileInterval = nil
        }
        
        if trackedByMonthsSwitch.on {
            tbMonths = true
            monthInterval = Int(monthIntervalField.text!)
        } else {
            tbMonths = false
            monthInterval = nil
        }
        
        let newItem = DefaultWorkItem(withName: name, trackingByMiles: tbMiles, miles: mileInterval, trackingByMonths: tbMonths, months: monthInterval)
        let encodedItem = NSKeyedArchiver.archivedDataWithRootObject(newItem)
        defaultItems?.append(encodedItem)
        
        // Save in NSUserDefaults
        NSUserDefaults.standardUserDefaults().setObject(defaultItems, forKey: "defaultItems")
        // Sort NSUserDefaults
        DefaultWorkItem.sortDefaultWorkItems()
    }
    
    func textFieldsAreValid() -> Bool {
        // Check for blank name field
        if nameField.text == "" {
            // Request user add a name
            let title = "Car Name"
            let message = "You must enter a name for the item"
            let ac = UIAlertController(title: title, message: message, preferredStyle: .Alert)
            let okAction = UIAlertAction(title: "OK", style: .Default, handler: { (action) -> Void in
                // Auto select car name field
                self.nameField?.becomeFirstResponder()
            })
            ac.addAction(okAction)
            presentViewController(ac, animated: true, completion: nil)
            
            return false
        }
        // Check for blank miles field with switch on
        else if trackedByMilesSwitch.on && Int(mileIntervalField.text!) == nil {
            // Request user add a miles number
            let title = "Mile Interval"
            let message = "You must enter a mile interval"
            let ac = UIAlertController(title: title, message: message, preferredStyle: .Alert)
            let okAction = UIAlertAction(title: "OK", style: .Default, handler: { (action) -> Void in
                // Auto select car miles field
                self.mileIntervalField.becomeFirstResponder()
            })
            ac.addAction(okAction)
            presentViewController(ac, animated: true, completion: nil)
            
            return false
        }
        // Check for blank months field with switch on
        else if trackedByMonthsSwitch.on && Int(monthIntervalField.text!) == nil {
            // Request user add a miles number
            let title = "Month Interval"
            let message = "You must enter a month interval"
            let ac = UIAlertController(title: title, message: message, preferredStyle: .Alert)
            let okAction = UIAlertAction(title: "OK", style: .Default, handler: { (action) -> Void in
                // Auto select car miles field
                self.monthIntervalField.becomeFirstResponder()
            })
            ac.addAction(okAction)
            presentViewController(ac, animated: true, completion: nil)
            
            return false
        }
        else {
            return true
        }
    }
    
    
    // Create toolbar for number text fields
    func addMilesToolbars() {
        let width = view.frame.size.width
        let toolbarRect = CGRectMake(0, 0, width, 50)
        let numberToolbar = UIToolbar(frame: toolbarRect)
        numberToolbar.barStyle = .Default
        let nextButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(resignAllResponders))
        nextButton.tintColor = UIColor.appNavyBlueColor()
        numberToolbar.items = [
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil),
            nextButton]
        numberToolbar.sizeToFit()
        mileIntervalField.inputAccessoryView = numberToolbar
        monthIntervalField.inputAccessoryView = numberToolbar
    }
    
    func resignAllResponders() {
        nameField.resignFirstResponder()
        mileIntervalField.resignFirstResponder()
        monthIntervalField.resignFirstResponder()
    }
}
